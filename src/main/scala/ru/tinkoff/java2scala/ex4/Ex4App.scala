package ru.tinkoff.java2scala.ex4

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import io.circe.generic.auto._
import org.http4s.circe._
import org.http4s.client._
import org.http4s.client.blaze._

import scala.concurrent.ExecutionContext.global

final case class MoneyAmount(currency: String, value: BigDecimal)
final case class Prices(buy: Option[MoneyAmount], sell: MoneyAmount)
final case class Symbol(ticker: String)
final case class TickerResponse(symbol: Symbol, prices: Prices)
final case class InvestResponse(payload: TickerResponse)

object Ex4App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    BlazeClientBuilder[IO](global)
      .resource
      .use { client =>
        printJsonResponse(client) >> printPlainResponse(client)
      }
      .map { _ =>
        ExitCode.Success
      }
  }

  private def printPlainResponse(client: Client[IO]): IO[Unit] = {
      client
        .expect[String]("https://api-invest.tinkoff.ru/trading/stocks/get?ticker=AAPL")
        .flatMap { response =>
          IO(println(response))
        }
  }

  private def printJsonResponse(client: Client[IO]): IO[Unit] = {
      client
        .expect[InvestResponse]("https://api-invest.tinkoff.ru/trading/stocks/get?ticker=AAPL")(jsonOf[IO, InvestResponse])
        .flatMap { response =>
          IO(println(response))
        }
  }
}
