CREATE TABLE todo (
    title TEXT PRIMARY KEY,
    description TEXT NOT NULL
);