package ru.tinkoff.java2scala.ex7.components

import cats.effect.{ConcurrentEffect, Resource, Timer}
import org.http4s.HttpRoutes
import org.http4s.implicits._
import org.http4s.server.{Router, Server}
import org.http4s.server.blaze.BlazeServerBuilder
import ru.tinkoff.java2scala.ex7.components.ConfigComponent.ApplicationConfig

object ServerComponent {
  def apply[F[_]: ConcurrentEffect: Timer](config: ApplicationConfig)(routes: HttpRoutes[F]): Resource[F, Server[F]] = {
    BlazeServerBuilder[F]
      .bindHttp(config.server.port, config.server.host)
      .withHttpApp(Router("/" -> routes).orNotFound)
      .resource
  }
}
