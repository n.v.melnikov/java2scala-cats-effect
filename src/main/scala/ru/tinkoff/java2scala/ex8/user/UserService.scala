package ru.tinkoff.java2scala.ex8.user

import java.util.UUID

import cats.effect.Sync
import cats.{Applicative, Monad}
import cats.syntax.functor._
import cats.syntax.flatMap._
import doobie.implicits._
import doobie.util.transactor.Transactor

import scala.util.control.NoStackTrace

class PersistenceException(message: String) extends Exception(message) with NoStackTrace

final case class User(id: String, username: String)

trait IdGenerator[F[_]] {
  def generate(): F[String]
}

class UserIdGenerator[F[_]: Applicative] extends IdGenerator[F] {
  override def generate(): F[String] = Applicative[F].pure(UUID.randomUUID().toString)
}

trait UserService[F[_]] {
  def register(username: String): F[User]
}

class UserServiceImpl[F[_]: Monad: Sync](idGenerator: IdGenerator[F], xa: Transactor[F]) extends UserService[F] {
  override def register(username: String): F[User] = {
    for {
      id <- idGenerator.generate()
      user = User(id, username)
      _ <- persist(user)
    } yield user
  }

  private def persist(user: User): F[Unit] = {
    sql"INSERT INTO users (id, username) VALUES (${user.id}, ${user.username})".update.run.transact(xa).flatMap {
      case 1 => Sync[F].delay(println("User persisted")) >> Applicative[F].pure(user)
      case _ => Sync[F].raiseError(new PersistenceException("Cannot persist user"))
    }
  }
}