package ru.tinkoff.java2scala.ex7.components

import cats.effect.{Resource, Sync}
import cats.syntax.flatMap._
import cats.{Applicative, Defer}
import org.http4s.HttpRoutes

object DatabaseConnection {
  def apply[F[_]: Applicative: Sync](): F[DatabaseConnection[F]] = {
    Sync[F].delay(println("DatabaseConnection: open")) >> Applicative[F].pure(new DatabaseConnection[F]())
  }
}

class DatabaseConnection[F[_]: Sync] {
  def close(): F[Unit] = Sync[F].delay(println("DatabaseConnection: close"))
}

object KafkaConnection {
  def apply[F[_]: Applicative: Sync](): F[KafkaConnection[F]] = {

    Sync[F].delay(println("KafkaConnection: open")) >> Applicative[F].pure(new KafkaConnection[F]())
  }
}

class KafkaConnection[F[_]: Sync] {
  def close(): F[Unit] = Sync[F].delay(println("KafkaConnection: close"))
}


object RoutesComponent {
  def controller[F[_]: Applicative: Defer](): Resource[F, Controller[F]] = Resource.pure[F, Controller[F]](new HelloWorldController[F])

  def stream[F[_]: Sync: Applicative](): Resource[F, KafkaConnection[F]] = Resource.make(KafkaConnection[F]())(_.close())
  def database[F[_]: Sync: Applicative](): Resource[F, DatabaseConnection[F]] = Resource.make(DatabaseConnection[F]())(_.close())

  def apply[F[_]: Applicative](controller: Controller[F]): Resource[F, HttpRoutes[F]] = Resource.pure[F, HttpRoutes[F]](controller.routes)
}