package ru.tinkoff.java2scala.ex8

import cats.effect.{Blocker, ExitCode, IO, IOApp}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import ru.tinkoff.java2scala.ex8.user.{UserIdGenerator, UserService, UserServiceImpl}

object SubscriptionApp extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      "jdbc:postgresql://localhost/demo",
      "demo",
      "demo",
      Blocker.liftExecutionContext(ExecutionContexts.synchronous)
    )

    val idGenerator = new UserIdGenerator[IO]
    val userService: UserService[IO] = new UserServiceImpl[IO](idGenerator, xa)

    userService.register("n.v.melnikov")
      .flatMap(user => IO(println(s"user: $user")))
      .map { _ =>
        ExitCode.Success
      }
  }
}
