name := "cats-app"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  // Logging
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  // Config
  "com.github.pureconfig" %% "pureconfig" % "0.12.3",
  // Cats effect
  "org.typelevel" %% "cats-effect" % "1.3.0",
  // Doobie
  "org.tpolecat" %% "doobie-core" % "0.8.8",
  // Postgres
  "org.tpolecat" %% "doobie-postgres" % "0.8.8",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  // Http4s
  "org.http4s" %% "http4s-dsl" % "0.21.2",
  "org.http4s" %% "http4s-blaze-server" % "0.21.2",
  "org.http4s" %% "http4s-blaze-client" % "0.21.2",
  "org.http4s" %% "http4s-circe" % "0.21.2",
  // Circe
  "io.circe" %% "circe-generic" % "0.13.0",
)

scalacOptions ++= Seq("-feature", "-deprecation", "-unchecked", "-language:postfixOps", "-language:higherKinds", "-Ypartial-unification")
