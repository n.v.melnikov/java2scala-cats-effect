package ru.tinkoff.java2scala.ex5

import cats.effect.{Clock, Concurrent, IO, Sync, Timer}
import org.slf4j._

// IO-based logger
object Logging {
  def apply(clazz: Class[_]): Logging = new Logging(LoggerFactory.getLogger(clazz))
}

class Logging(underlying: Logger) {
  def info(message: String): IO[Unit] = IO(underlying.info(message))
}

// Typeclass-based logger
object Log {
  def apply[F[_]: Sync](clazz: Class[_]): Log[F] = new Log[F](LoggerFactory.getLogger(clazz))
}

class Log[F[_]: Sync](underlying: Logger) {
  def info(message: String): F[Unit] = Sync[F].delay(underlying.info(message))
}