package ru.tinkoff.java2scala.ex7

import cats.Defer
import cats.effect.{ExitCode, IO, IOApp}
import ru.tinkoff.java2scala.ex7.components._

object Ex7App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val resources = for {
      _ <- RoutesComponent.stream[IO]()
      _ <- RoutesComponent.database[IO]()
    } yield ()

    resources.use { _ =>
      IO(println("Application time..."))
    }.map { _ =>
      ExitCode.Success
    }
  }
}
