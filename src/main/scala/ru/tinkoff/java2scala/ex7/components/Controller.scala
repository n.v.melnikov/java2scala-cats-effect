package ru.tinkoff.java2scala.ex7.components

import cats.{Applicative, Defer}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

trait Controller[F[_]] {
  val routes: HttpRoutes[F]
}

abstract class Http4sController[F[_]: Applicative: Defer] extends Controller[F] with Http4sDsl[F]

class HelloWorldController[F[_]: Applicative: Defer] extends Http4sController[F] {
  override val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root => Ok("Hello, World!")
  }
}
