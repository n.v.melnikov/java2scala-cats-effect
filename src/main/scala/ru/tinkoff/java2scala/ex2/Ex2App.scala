package ru.tinkoff.java2scala.ex2

import cats.effect.{ExitCode, IO, IOApp}
import pureconfig._
import pureconfig.generic.auto._ // это нужно, хоть идея и говорит, что не нужно

final case class SessionController(parallelism: Int)
final case class ServerConfig(hostName: String, port: Option[Int], sessionController: SessionController)
final case class AppConfig(server: ServerConfig)

object Ex2App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val config = ConfigSource.string(
      """
        | server {
        |   host-name: "0.0.0.0"
        |   session-controller {
        |     parallelism: 4
        |   }
        | }
        |""".stripMargin
    ).load[AppConfig] match {
      case Left(err) => IO.raiseError(new Exception(s"Cannot read config: $err"))
      case Right(config) => IO(config)
    }

    config
      .flatMap { cfg =>
        IO(println(cfg.server))
      }
      .map { _ =>
        ExitCode.Success
      }
  }
}
