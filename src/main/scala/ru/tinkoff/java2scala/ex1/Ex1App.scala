package ru.tinkoff.java2scala.ex1

import java.time.Instant

import cats.effect._
import cats.implicits._
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze._

case class CurrentTimestamp(time: Long)

object SomeRequest {
  implicit val decoder = jsonOf[IO, SomeRequest]
}
case class SomeRequest(value: String)

case class SomeResponse(value: String)

object Ex1App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val helloWorldService: HttpRoutes[IO] = HttpRoutes.of[IO] {
      case GET -> Root / "hello" / name => Ok(s"Hello, $name")

      case GET -> Root / "time" => Ok(CurrentTimestamp(Instant.now().toEpochMilli).asJson)

      case req @ POST -> Root / "reply" => for {
        someRequest <- req.as[SomeRequest]
        response <- Ok(SomeResponse(someRequest.value).asJson)
      } yield response
    }

    BlazeServerBuilder[IO]
      .bindHttp(8080, "localhost")
      .withHttpApp(Router("/" -> helloWorldService).orNotFound)
      .resource
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }
}